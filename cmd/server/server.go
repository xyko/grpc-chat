package main

import (
	"fmt"
	"log"
	"net"

	"gitlab.com/xyko/grpc-chat/internal/server/contactssrv"
	"gitlab.com/xyko/grpc-chat/pkg/db/fscnn"
	"gitlab.com/xyko/grpc-chat/pkg/proto/pbcontacts"
	"google.golang.org/grpc"
)

const (
	ServerEndpoint = ":5557"
)

func main() {
	fmt.Printf("Listening to %s\n", ServerEndpoint)

	// listen to tcp port
	lis, err := net.Listen("tcp", ServerEndpoint)
	if err != nil {
		log.Fatal("Cannot listen to %s: %v", ServerEndpoint, err)
	}

	// database connections
	dbcnn := &fscnn.ContactsCnn{}

	// grpc server
	server := grpc.NewServer()
	pbcontacts.RegisterContactsServer(server, contactssrv.NewContactsServerImplementation(dbcnn))
	if err := server.Serve(lis); err != nil {
		log.Fatal("Error serving gRPC: %v", err)
	}
}
