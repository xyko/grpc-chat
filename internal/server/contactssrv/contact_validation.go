package contactssrv

import (
	"errors"

	"gitlab.com/xyko/grpc-chat/pkg/proto/pbcontacts"
)

var (
	ErrIllegalName = errors.New("Contact name is empty")
)

func isContactValid(c *pbcontacts.Contact) error {
	if c.Name == "" {
		return ErrIllegalName
	}

	return nil
}
