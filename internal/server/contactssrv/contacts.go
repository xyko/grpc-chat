package contactssrv

import (
	"context"
	"fmt"
	"gitlab.com/xyko/grpc-chat/pkg/proto/pbcontacts"
)

func NewContactsServerImplementation(db ContactDb) *ContactsServerImplementation {
	return &ContactsServerImplementation{
		Db: db,
	}
}

type ContactsServerImplementation struct {
	Db ContactDb
}

func (me *ContactsServerImplementation) Add(context context.Context, req *pbcontacts.AddRequest) (*pbcontacts.AddResponse, error) {
	if err := isContactValid(req.Contact); err != nil {
		return nil, err
	}

	if req.Contact.Id != "" {
		return nil, fmt.Errorf("you cannot add a contact that already have an ID")
	}

	_, err := me.Db.New(req.Contact)
	if err != nil {
		return nil, fmt.Errorf("db error adding contact: %v", err)
	}

	return &pbcontacts.AddResponse{}, nil
}

func (me *ContactsServerImplementation) ConnectToContacts(*pbcontacts.ConnectToContactsRequest, pbcontacts.Contacts_ConnectToContactsServer) error {
	return fmt.Errorf("not implemented")
}

func (me *ContactsServerImplementation) Invite(context.Context, *pbcontacts.InviteRequest) (*pbcontacts.InviteResponse, error) {
	return nil, fmt.Errorf("not implemented")
}
