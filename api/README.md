# /cmd
This directory contains protobuf files.

## Rebuilding
After changing any of these files you must regenerate the files at pkg/proto. For that use gnu make:

    make -j

