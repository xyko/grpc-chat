package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/xyko/grpc-chat/pkg/proto/pbcontacts"
	"google.golang.org/grpc"
)

var (
	hostName      string
	port          int
	totalRequests int
	clients       int

	currentRequest int32
)

func init() {
	flag.StringVar(&hostName, "host", "127.0.0.1", "host to connect")
	flag.IntVar(&port, "port", 5557, "TCP port to connect")
	flag.IntVar(&totalRequests, "requests", 1, "Total number of requests to send")
	flag.IntVar(&clients, "clients", 1, "Number of simultaneous connections")
	flag.Parse()
}

func main() {
	start := time.Now()

	endpoint := fmt.Sprintf("%s:%d", hostName, port)
	var wg sync.WaitGroup

	log.Printf("Starting %d clients for handling %d requests\n", clients, totalRequests)

	for cc := 0; cc < clients; cc++ {
		wg.Add(1)
		go func(cc int) {
			defer wg.Done()

			cnn, err := grpc.Dial(endpoint, grpc.WithInsecure())
			if err != nil {
				log.Printf("C%d Cannot connect to backend server %s: %v\n", cc, endpoint, err)
				return
			}
			defer cnn.Close()

			client := pbcontacts.NewContactsClient(cnn)

			for {
				if atomic.AddInt32(&currentRequest, 1) > int32(totalRequests) {
					return
				}

				_, err := client.Add(context.Background(), &pbcontacts.AddRequest{
					Contact: &pbcontacts.Contact{
						Name: "Luciane dos Santos Clazer",
						Email: []*pbcontacts.Email{
							&pbcontacts.Email{Desc: "Personal", Value: "luciane@kurpiel.eng.br"},
						},
						Notes: "Mulher linda",
					},
				})
				if err != nil {
					log.Printf("C%d Error requesting add command to %s: %v\n", cc, endpoint, err)
				}
			}
		}(cc)
	}
	wg.Wait()

	dur := time.Since(start).Seconds()
	log.Printf("Processed %d requests in %f seconds at %f req/s", totalRequests,
		dur, float64(totalRequests)/dur)
}
