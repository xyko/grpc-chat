# build stage
FROM golang:alpine AS build-env
ADD . /src
RUN apk add --no-cache git; \
	cd /src/cmd/server; CGO_ENABLED=0 go build -o server

# server
FROM alpine
WORKDIR /app
COPY --from=build-env /src/cmd/server/server /app/
EXPOSE 5557/tcp
ENTRYPOINT ./server

