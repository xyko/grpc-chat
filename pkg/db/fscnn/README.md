# Driver for Google FireStore
This package contains the implementation of the database interface for storing
contacts and messages on Google FireStore, that is a scallable NoSQL database
available on Google Cloud.
