package fscnn

import (
	"errors"

	"gitlab.com/xyko/grpc-chat/pkg/proto/pbcontacts"
)

type ContactsCnn struct {
}

func (me *ContactsCnn) New(contact *pbcontacts.Contact) (*pbcontacts.Contact, error) {
	return nil, errors.New("not implemented")
}

func (me *ContactsCnn) Update(contact *pbcontacts.Contact) error {
	return errors.New("not implemented")
}
