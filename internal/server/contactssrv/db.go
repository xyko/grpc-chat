package contactssrv

import (
	"gitlab.com/xyko/grpc-chat/pkg/proto/pbcontacts"
)

type ContactDb interface {
	New(contact *pbcontacts.Contact) (*pbcontacts.Contact, error)
	Update(contact *pbcontacts.Contact) error
}
