name:=gcr.io/fdk-pub-docker/grpc-chat-server
ver:=0.2

.PHONY: server
server:
	docker build -t ${name}:${ver} .

.PHONY: push-server
push-server: server
	docker push ${name}:${ver}

.PHONY: run-server
run-server: server
	docker run -p 5557:5557/tcp -it --rm ${name}:${ver}

.PHONY: run-client
run-client:
	go run cmd/client/*.go

