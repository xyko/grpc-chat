// Code generated by protoc-gen-go. DO NOT EDIT.
// source: contact.proto

package pbcontacts

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type Contact struct {
	Id                   string               `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string               `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Email                []*Email             `protobuf:"bytes,3,rep,name=email,proto3" json:"email,omitempty"`
	Phones               []*Phone             `protobuf:"bytes,4,rep,name=phones,proto3" json:"phones,omitempty"`
	Address              []*Address           `protobuf:"bytes,5,rep,name=address,proto3" json:"address,omitempty"`
	Urls                 []*URLs              `protobuf:"bytes,6,rep,name=urls,proto3" json:"urls,omitempty"`
	Birthday             *timestamp.Timestamp `protobuf:"bytes,7,opt,name=birthday,proto3" json:"birthday,omitempty"`
	Notes                string               `protobuf:"bytes,8,opt,name=notes,proto3" json:"notes,omitempty"`
	XXX_NoUnkeyedLiteral struct{}             `json:"-"`
	XXX_unrecognized     []byte               `json:"-"`
	XXX_sizecache        int32                `json:"-"`
}

func (m *Contact) Reset()         { *m = Contact{} }
func (m *Contact) String() string { return proto.CompactTextString(m) }
func (*Contact) ProtoMessage()    {}
func (*Contact) Descriptor() ([]byte, []int) {
	return fileDescriptor_a5036fff2565fb15, []int{0}
}

func (m *Contact) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Contact.Unmarshal(m, b)
}
func (m *Contact) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Contact.Marshal(b, m, deterministic)
}
func (m *Contact) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Contact.Merge(m, src)
}
func (m *Contact) XXX_Size() int {
	return xxx_messageInfo_Contact.Size(m)
}
func (m *Contact) XXX_DiscardUnknown() {
	xxx_messageInfo_Contact.DiscardUnknown(m)
}

var xxx_messageInfo_Contact proto.InternalMessageInfo

func (m *Contact) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *Contact) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Contact) GetEmail() []*Email {
	if m != nil {
		return m.Email
	}
	return nil
}

func (m *Contact) GetPhones() []*Phone {
	if m != nil {
		return m.Phones
	}
	return nil
}

func (m *Contact) GetAddress() []*Address {
	if m != nil {
		return m.Address
	}
	return nil
}

func (m *Contact) GetUrls() []*URLs {
	if m != nil {
		return m.Urls
	}
	return nil
}

func (m *Contact) GetBirthday() *timestamp.Timestamp {
	if m != nil {
		return m.Birthday
	}
	return nil
}

func (m *Contact) GetNotes() string {
	if m != nil {
		return m.Notes
	}
	return ""
}

type ContactLink struct {
	Link                 uint64   `protobuf:"varint,1,opt,name=link,proto3" json:"link,omitempty"`
	Key                  uint64   `protobuf:"varint,2,opt,name=key,proto3" json:"key,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ContactLink) Reset()         { *m = ContactLink{} }
func (m *ContactLink) String() string { return proto.CompactTextString(m) }
func (*ContactLink) ProtoMessage()    {}
func (*ContactLink) Descriptor() ([]byte, []int) {
	return fileDescriptor_a5036fff2565fb15, []int{1}
}

func (m *ContactLink) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ContactLink.Unmarshal(m, b)
}
func (m *ContactLink) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ContactLink.Marshal(b, m, deterministic)
}
func (m *ContactLink) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ContactLink.Merge(m, src)
}
func (m *ContactLink) XXX_Size() int {
	return xxx_messageInfo_ContactLink.Size(m)
}
func (m *ContactLink) XXX_DiscardUnknown() {
	xxx_messageInfo_ContactLink.DiscardUnknown(m)
}

var xxx_messageInfo_ContactLink proto.InternalMessageInfo

func (m *ContactLink) GetLink() uint64 {
	if m != nil {
		return m.Link
	}
	return 0
}

func (m *ContactLink) GetKey() uint64 {
	if m != nil {
		return m.Key
	}
	return 0
}

type Email struct {
	Desc                 string   `protobuf:"bytes,1,opt,name=desc,proto3" json:"desc,omitempty"`
	Value                string   `protobuf:"bytes,2,opt,name=value,proto3" json:"value,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Email) Reset()         { *m = Email{} }
func (m *Email) String() string { return proto.CompactTextString(m) }
func (*Email) ProtoMessage()    {}
func (*Email) Descriptor() ([]byte, []int) {
	return fileDescriptor_a5036fff2565fb15, []int{2}
}

func (m *Email) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Email.Unmarshal(m, b)
}
func (m *Email) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Email.Marshal(b, m, deterministic)
}
func (m *Email) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Email.Merge(m, src)
}
func (m *Email) XXX_Size() int {
	return xxx_messageInfo_Email.Size(m)
}
func (m *Email) XXX_DiscardUnknown() {
	xxx_messageInfo_Email.DiscardUnknown(m)
}

var xxx_messageInfo_Email proto.InternalMessageInfo

func (m *Email) GetDesc() string {
	if m != nil {
		return m.Desc
	}
	return ""
}

func (m *Email) GetValue() string {
	if m != nil {
		return m.Value
	}
	return ""
}

type Phone struct {
	Country              string   `protobuf:"bytes,1,opt,name=country,proto3" json:"country,omitempty"`
	Desc                 string   `protobuf:"bytes,2,opt,name=desc,proto3" json:"desc,omitempty"`
	Value                string   `protobuf:"bytes,3,opt,name=value,proto3" json:"value,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Phone) Reset()         { *m = Phone{} }
func (m *Phone) String() string { return proto.CompactTextString(m) }
func (*Phone) ProtoMessage()    {}
func (*Phone) Descriptor() ([]byte, []int) {
	return fileDescriptor_a5036fff2565fb15, []int{3}
}

func (m *Phone) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Phone.Unmarshal(m, b)
}
func (m *Phone) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Phone.Marshal(b, m, deterministic)
}
func (m *Phone) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Phone.Merge(m, src)
}
func (m *Phone) XXX_Size() int {
	return xxx_messageInfo_Phone.Size(m)
}
func (m *Phone) XXX_DiscardUnknown() {
	xxx_messageInfo_Phone.DiscardUnknown(m)
}

var xxx_messageInfo_Phone proto.InternalMessageInfo

func (m *Phone) GetCountry() string {
	if m != nil {
		return m.Country
	}
	return ""
}

func (m *Phone) GetDesc() string {
	if m != nil {
		return m.Desc
	}
	return ""
}

func (m *Phone) GetValue() string {
	if m != nil {
		return m.Value
	}
	return ""
}

type Address struct {
	Desc                 string   `protobuf:"bytes,1,opt,name=desc,proto3" json:"desc,omitempty"`
	Value                string   `protobuf:"bytes,2,opt,name=value,proto3" json:"value,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Address) Reset()         { *m = Address{} }
func (m *Address) String() string { return proto.CompactTextString(m) }
func (*Address) ProtoMessage()    {}
func (*Address) Descriptor() ([]byte, []int) {
	return fileDescriptor_a5036fff2565fb15, []int{4}
}

func (m *Address) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Address.Unmarshal(m, b)
}
func (m *Address) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Address.Marshal(b, m, deterministic)
}
func (m *Address) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Address.Merge(m, src)
}
func (m *Address) XXX_Size() int {
	return xxx_messageInfo_Address.Size(m)
}
func (m *Address) XXX_DiscardUnknown() {
	xxx_messageInfo_Address.DiscardUnknown(m)
}

var xxx_messageInfo_Address proto.InternalMessageInfo

func (m *Address) GetDesc() string {
	if m != nil {
		return m.Desc
	}
	return ""
}

func (m *Address) GetValue() string {
	if m != nil {
		return m.Value
	}
	return ""
}

type URLs struct {
	Desc                 string   `protobuf:"bytes,1,opt,name=desc,proto3" json:"desc,omitempty"`
	Value                string   `protobuf:"bytes,2,opt,name=value,proto3" json:"value,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *URLs) Reset()         { *m = URLs{} }
func (m *URLs) String() string { return proto.CompactTextString(m) }
func (*URLs) ProtoMessage()    {}
func (*URLs) Descriptor() ([]byte, []int) {
	return fileDescriptor_a5036fff2565fb15, []int{5}
}

func (m *URLs) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_URLs.Unmarshal(m, b)
}
func (m *URLs) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_URLs.Marshal(b, m, deterministic)
}
func (m *URLs) XXX_Merge(src proto.Message) {
	xxx_messageInfo_URLs.Merge(m, src)
}
func (m *URLs) XXX_Size() int {
	return xxx_messageInfo_URLs.Size(m)
}
func (m *URLs) XXX_DiscardUnknown() {
	xxx_messageInfo_URLs.DiscardUnknown(m)
}

var xxx_messageInfo_URLs proto.InternalMessageInfo

func (m *URLs) GetDesc() string {
	if m != nil {
		return m.Desc
	}
	return ""
}

func (m *URLs) GetValue() string {
	if m != nil {
		return m.Value
	}
	return ""
}

func init() {
	proto.RegisterType((*Contact)(nil), "pbcontacts.Contact")
	proto.RegisterType((*ContactLink)(nil), "pbcontacts.ContactLink")
	proto.RegisterType((*Email)(nil), "pbcontacts.Email")
	proto.RegisterType((*Phone)(nil), "pbcontacts.Phone")
	proto.RegisterType((*Address)(nil), "pbcontacts.Address")
	proto.RegisterType((*URLs)(nil), "pbcontacts.URLs")
}

func init() { proto.RegisterFile("contact.proto", fileDescriptor_a5036fff2565fb15) }

var fileDescriptor_a5036fff2565fb15 = []byte{
	// 341 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x94, 0x90, 0xcb, 0x4e, 0xe3, 0x30,
	0x14, 0x86, 0xd5, 0x5c, 0x9a, 0xce, 0xa9, 0x66, 0xd4, 0x31, 0x2c, 0xac, 0x6e, 0xa8, 0x22, 0x24,
	0xca, 0x82, 0x14, 0x5a, 0x89, 0x3d, 0x42, 0xac, 0xe8, 0x02, 0x59, 0xf0, 0x00, 0xb9, 0x98, 0xd6,
	0x6a, 0x62, 0x47, 0xb1, 0x83, 0xd4, 0xf7, 0xe1, 0x41, 0x91, 0x8f, 0x93, 0x52, 0x09, 0x16, 0xb0,
	0xfb, 0x8f, 0xff, 0xcf, 0xc7, 0xc9, 0x07, 0x7f, 0x73, 0x25, 0x4d, 0x9a, 0x9b, 0xa4, 0x6e, 0x94,
	0x51, 0x04, 0xea, 0xac, 0x3b, 0xd0, 0xd3, 0xb3, 0x8d, 0x52, 0x9b, 0x92, 0x2f, 0xb0, 0xc9, 0xda,
	0xd7, 0x85, 0x11, 0x15, 0xd7, 0x26, 0xad, 0x6a, 0x07, 0xc7, 0xef, 0x1e, 0x44, 0xf7, 0x8e, 0x26,
	0xff, 0xc0, 0x13, 0x05, 0x1d, 0xcc, 0x06, 0xf3, 0x3f, 0xcc, 0x13, 0x05, 0x21, 0x10, 0xc8, 0xb4,
	0xe2, 0xd4, 0xc3, 0x13, 0xcc, 0xe4, 0x02, 0x42, 0x5e, 0xa5, 0xa2, 0xa4, 0xfe, 0xcc, 0x9f, 0x8f,
	0x97, 0xff, 0x93, 0xcf, 0xc7, 0x92, 0x07, 0x5b, 0x30, 0xd7, 0x93, 0x4b, 0x18, 0xd6, 0x5b, 0x25,
	0xb9, 0xa6, 0xc1, 0x57, 0xf2, 0xc9, 0x36, 0xac, 0x03, 0xc8, 0x15, 0x44, 0x69, 0x51, 0x34, 0x5c,
	0x6b, 0x1a, 0x22, 0x7b, 0x72, 0xcc, 0xde, 0xb9, 0x8a, 0xf5, 0x0c, 0x39, 0x87, 0xa0, 0x6d, 0x4a,
	0x4d, 0x87, 0xc8, 0x4e, 0x8e, 0xd9, 0x17, 0xb6, 0xd6, 0x0c, 0x5b, 0x72, 0x0b, 0xa3, 0x4c, 0x34,
	0x66, 0x5b, 0xa4, 0x7b, 0x1a, 0xcd, 0x06, 0xf3, 0xf1, 0x72, 0x9a, 0x38, 0x19, 0x49, 0x2f, 0x23,
	0x79, 0xee, 0x65, 0xb0, 0x03, 0x4b, 0x4e, 0x21, 0x94, 0xca, 0x70, 0x4d, 0x47, 0xf8, 0xd7, 0x6e,
	0x88, 0x57, 0x30, 0xee, 0x2c, 0xad, 0x85, 0xdc, 0x59, 0x33, 0xa5, 0x90, 0x3b, 0x74, 0x15, 0x30,
	0xcc, 0x64, 0x02, 0xfe, 0x8e, 0xef, 0x51, 0x56, 0xc0, 0x6c, 0x8c, 0x6f, 0x20, 0x44, 0x25, 0x16,
	0x2f, 0xb8, 0xce, 0x3b, 0xb5, 0x98, 0xed, 0x3b, 0x6f, 0x69, 0xd9, 0xf6, 0x76, 0xdd, 0x10, 0x3f,
	0x42, 0x88, 0x6e, 0x08, 0x85, 0x28, 0x57, 0xad, 0x34, 0xcd, 0xbe, 0xbb, 0xd5, 0x8f, 0x87, 0x65,
	0xde, 0x77, 0xcb, 0xfc, 0xe3, 0x65, 0x2b, 0x88, 0x3a, 0x79, 0xbf, 0xf8, 0x82, 0x6b, 0x08, 0xac,
	0xc5, 0x9f, 0xdf, 0xc8, 0x86, 0xe8, 0x73, 0xf5, 0x11, 0x00, 0x00, 0xff, 0xff, 0x13, 0xb6, 0x35,
	0x95, 0x87, 0x02, 0x00, 0x00,
}
