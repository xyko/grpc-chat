module gitlab.com/xyko/grpc-chat

require (
	github.com/golang/protobuf v1.2.1-0.20181128192352-1d3f30b51784
	google.golang.org/grpc v1.17.0
)
